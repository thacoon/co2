CO2
===

[![pipeline status](https://gitlab.com/thacoon/co2/badges/master/pipeline.svg)](https://gitlab.com/thacoon/co2/commits/master)

## Calculations

For more information about the calculations, see the [wiki](https://gitlab.com/thacoon/co2/wikis/home).

## Development

### Upgrade

To upgrade the packages run `./node_modules/npm-check-updates/bin/npm-check-updates -u` and then `npm install`.

### Run in browser

To run the extension in a Firefox browser run `npm run firefox`.

### Tests

Run the tests with `npm run test:karma`.

### Generate minified Tailwind css file

Run the build command with `npm run tailwind:build`. 