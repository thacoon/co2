function getStats(name) {
    const stats = localStorage.getItem(name);
    const statsJson = null == stats ? {} : JSON.parse(stats);
    return statsJson;
}

function displaySearchStats(site) {
    const statsSearchJson = getStats('search');
    let googleSearchEmissions = undefined === statsSearchJson['google'] ? 0 : parseFloat(statsSearchJson['google']);
    let emissions_google_search = googleSearchEmissions.toFixed(3) || 0.000;

    console.log('Google Search Emissions: ' + googleSearchEmissions);
    document.getElementById('id_emissions_google_search').innerHTML = emissions_google_search.toString();
    document.getElementById('id_emissions_total_search_engines').innerHTML = emissions_google_search.toString();

    const statsStreamingJson = getStats('streaming');
    let youtubeSearchEmissions = undefined === statsStreamingJson['youtube'] ? 0 : parseFloat(statsStreamingJson['youtube']);
    let emissions_youtube = youtubeSearchEmissions.toFixed(3) || 0.000;

    console.log('Youtube Emissions: ' + youtubeSearchEmissions);
    document.getElementById('id_emissions_streaming_youtube').innerHTML = emissions_youtube.toString();
    document.getElementById('id_emissions_total_streaming').innerHTML = emissions_youtube.toString();

    document.getElementById('id_emissions_total').innerHTML = emissions_google_search;
}

displaySearchStats();