// There are no wildcard for top level domains (TLD), e.g. .com, .de
let google = {
    'urls': ['*://*.google.com/search?*', '*://*.google.de/search?*']
};
let youtube = {
    'urls': ['*://*.youtube.com/*', '*://*.youtube.de/*', '*://*.googlevideo.com/*']
};

function googleEmissionsPerkWh() {
    let shareCoal = 0.14;
    let shareNaturalGas = 0.15;

    let emissionsCoal = 0.325;
    let emissionsNaturalGas = 0.181;

    return shareCoal * emissionsCoal +
        shareNaturalGas * emissionsNaturalGas;
}

function calcGoogleSearchCO2Emissions(number_of_searches) {
    let emissionsPerkWh = googleEmissionsPerkWh();
    return emissionsPerkWh * 0.0003 * number_of_searches;
}

function calcGoogleByteCO2Emissions(bytes) {
    let emissionsPerkWh = googleEmissionsPerkWh();
    return emissionsPerkWh * bytes * 7.2e-11;
}

function isContentLength(responseHeader) {
    return responseHeader.name === 'content-length' || responseHeader.name === 'Content-Length';
}

function headersStreamingListener(requestDetails) {
    let contentLength = requestDetails.responseHeaders.find(isContentLength).value;

    if(contentLength === undefined) {
        return;
    }

    let emissions = calcGoogleByteCO2Emissions(contentLength);

    const stats = localStorage.getItem('streaming');
    const statsJson = null === stats ? {} : JSON.parse(stats);

    statsJson['youtube'] = null == statsJson['youtube'] ? 0 : statsJson['youtube'];
    statsJson['youtube'] += emissions;

    localStorage.setItem('streaming', JSON.stringify(statsJson));
}

chrome.webRequest.onCompleted.addListener( (evt) => {
        let emissions = calcGoogleSearchCO2Emissions(1);

        const stats = localStorage.getItem('search');
        const statsJson = null == stats ? {} : JSON.parse(stats);

        statsJson['google'] += emissions;

        localStorage.setItem('search', JSON.stringify(statsJson));
    },
    {urls: google['urls']}
);

chrome.webRequest.onHeadersReceived.addListener(
    headersStreamingListener,
    {urls: youtube['urls']},
    ['responseHeaders']
);
