describe('background-script', function () {
    describe('CO2 calculations', function () {
        it('calcs the correct CO2 emisssions for 1 google searches', function () {
            expect(calcGoogleSearchCO2Emissions(1)).to.be.closeTo(0.000021795, 1e-9);
        });

        it('calcs the correct CO2 emisssions for 1000 google searches', function () {
            expect(calcGoogleSearchCO2Emissions(1000)).to.be.closeTo(0.021795, 1e-9);
        });

        it('calcs the correct CO2 emssisions for 1GB of youtube streaming', function () {
            expect(calcGoogleByteCO2Emissions(1e9)).to.be.closeTo(0.0052308, 1e-9);
        });

        it('calcs the correct CO2 emissions for header with fictional 1448112 content byte', function () {
            const requestMock = {
                'responseHeaders' : [{name: 'Content-Length', value: 1448112}]
            };
            headersStreamingListener(requestMock);
            let storedEmissions = JSON.parse(localStorage.getItem('streaming'))['youtube'];
            expect(storedEmissions).to.be.closeTo(7.5747842496e-06, 1e-9);
        });
    });
});